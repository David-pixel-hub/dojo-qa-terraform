variable "ami" {
  type        = string
  description = "Ubuntu AMI en la region eu-central-1"
  default     = "ami-0faab6bdbac9486fb"
}

variable "instance_type" {
  type        = string
  description = "tipo de instancia"
  default     = "t2.medium"
}

variable "name_tag" {
  type        = string
  description = "nombre de la instancia ec2"
  default     = "instancia ec2"
}