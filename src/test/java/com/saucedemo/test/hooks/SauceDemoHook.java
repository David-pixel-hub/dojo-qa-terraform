package com.saucedemo.test.hooks;

import io.cucumber.java.Before;
import net.serenitybdd.annotations.Managed;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.openqa.selenium.*;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class SauceDemoHook {
    @Managed
    WebDriver driver;

    @Before
    public void setupActor() {
        EdgeOptions options = new EdgeOptions();
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--headless=new");
        options.addArguments("--no-sandbox");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("disable-default-apps");
        options.addArguments("test-type=browser");
        driver = new EdgeDriver(options);
        OnStage.setTheStage(new OnlineCast());
        Actor clienteamigo = OnStage.theActorCalled("usuario");
        clienteamigo.can(BrowseTheWeb.with(driver));
        driver.manage().window().maximize();
    }
}
