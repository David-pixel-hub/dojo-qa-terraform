package com.saucedemo.main.question;

import com.saucedemo.main.ui.LoginPage;
import com.saucedemo.main.utils.Loggin;
import groovy.util.logging.Log;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.questions.Visibility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoginQuestion implements Question<Boolean> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginQuestion.class);



    public static Question<Boolean> Validate() {
        return new LoginQuestion();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String mensajeInformativo = Loggin.formatMessageInformation("validando la visualizacion del inventario de productos");
        LOGGER.info(mensajeInformativo);

        return LoginPage.INVENTORY_LIST.isVisibleFor(actor);
//        return Text.of(Page.TARGET).answeredBy(actor).equals(inventory);
    }
}
