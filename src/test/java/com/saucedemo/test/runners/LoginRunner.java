package com.saucedemo.test.runners;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/java/resources/features/InicioDeSesion/login.feature",
        glue = {"com.saucedemo.test.stepsdefinitions","com.saucedemo.test.hooks"},
        snippets = CucumberOptions.SnippetType.CAMELCASE
)

public class LoginRunner {
}
