package com.saucedemo.main.task;

import com.saucedemo.main.utils.Loggin;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.actions.Open;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GotoTask implements Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(GotoTask.class);

    public static Performable Saucedemo() {
        return instrumented(GotoTask.class);
    }

    @Override
    @Step("{0} intenta realizar la tarea de navegar a la pagina de saucedemo")
    public <T extends Actor> void performAs(T actor) {
        String mensajeInformativo = Loggin.formatMessageInformation("abriendo url de saucedemo");
        LOGGER.info(mensajeInformativo);
        actor.attemptsTo(
                Open.url("https://www.saucedemo.com/v1/index.html")
        );
    }
}
