resource "aws_instance" "my_vm" {
  ami           = var.ami //Ubuntu AMI
  instance_type = var.instance_type
  user_data     = file("recursos.sh")


  tags = {
    Name = var.name_tag,
  }
}