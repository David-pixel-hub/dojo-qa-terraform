#! /bin/bash
sudo -i
apt update -y
apt upgrade -y
sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
sudo apt install -y libc6-x32 libc6-i386
wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.deb
sudo dpkg -i jdk-17_linux-x64_bin.deb
wget https://downloads.gradle-dn.com/distributions/gradle-8.0.2-bin.zip
wget https://services.gradle.org/distributions/gradle-8.0.2-bin.zip
apt install wget unzip -y
unzip gradle-8.0.2-bin.zip -d /opt/gradle
printf "export GRADLE_HOME=/opt/gradle/gradle-8.0.2\nexport PATH=\${GRADLE_HOME}/bin:\${PATH}" >> /etc/profile.d/gradle.sh
chmod +x /etc/profile.d/gradle.sh
source /etc/profile.d/gradle.sh
cd
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-stable.list'
sudo rm microsoft.gpg
sudo apt update && sudo apt install microsoft-edge-stable -y
sudo apt-get install -f -y
sudo microsoft-edge
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
gitlab-runner register  --non-interactive --url https://gitlab.com --token glrt-jPe8Bgx8Yhgjw_54i97P --name runner_terraform_edgar --executor shell
sudo rm /home/gitlab-runner/.bash_logout
sudo gitlab-runner restart