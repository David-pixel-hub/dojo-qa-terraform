package com.saucedemo.test.stepsdefinitions;

import com.saucedemo.main.question.LoginQuestion;
import com.saucedemo.main.task.GotoTask;
import com.saucedemo.main.task.LoginTask;
import io.cucumber.java.es.*;
import net.serenitybdd.screenplay.questions.Visibility;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class LoginStepDefinitions {
    @Dado("usuario fue capaz de situarse en la pagina de prueba")
    public void usuarioFueCapazDeSituarseEnLaPaginaDePrueba() {
        theActorInTheSpotlight().wasAbleTo(
                GotoTask.Saucedemo()
        );
    }

    @Cuando("usuario intenta ingresar credenciales validas")
    public void usuarioIntentaIngresarCredencialesValidas() {
        theActorInTheSpotlight().attemptsTo(
                LoginTask.EnterCredentials("standard_user","secret_sauce")
        );
    }

    @Entonces("usuario deberia inicicar sesion correctamente")
    public void usuarioDeberiaInicicarSesionCorrectamente() {
        theActorInTheSpotlight().should(
                seeThat(theActorInTheSpotlight()+"deberia visualizar el inventario de productos", LoginQuestion.Validate())
        );
    }
}
