output "public_ip" {
  value       = aws_instance.my_vm.public_ip
  description = "IP publica de la instancia ec2"
}

output "instance_id" {
  value       = aws_instance.my_vm.id
  description = "Identificador de la instancia"
}