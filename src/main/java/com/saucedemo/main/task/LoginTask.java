package com.saucedemo.main.task;

import com.saucedemo.main.ui.LoginPage;
import com.saucedemo.main.utils.Loggin;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginTask implements Task {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginTask.class);
    private final String username;
    private final String password;

    public LoginTask(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static Performable EnterCredentials(String username, String password) {
        return instrumented(LoginTask.class, username, password);
    }

    @Override
    @Step("{0} intenta realizar la tarea de ingresar sus credenciales")
    public <T extends Actor> void performAs(T actor) {
        String mensajeInformativo = Loggin.formatMessageInformation("intentando iniciar sesion con credenciales validas");
        LOGGER.info(mensajeInformativo);
        actor.attemptsTo(
                Enter.theValue(username).into(LoginPage.USERNAME_INPUT),
                Enter.theValue(password).into(LoginPage.PASSWORD_INPUT),
                Click.on(LoginPage.LOGIN_BUTTON)
        );
    }
}
