package com.saucedemo.main.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Loggin {

    private static final Logger LOGGER = LoggerFactory.getLogger(Loggin.class);
    private static final String MESSAGE_INFORMATION = "\n%s\n----------------------------------------------------";

    public static String formatMessageInformation(String message) {
        return String.format(MESSAGE_INFORMATION, message);
    }
}
