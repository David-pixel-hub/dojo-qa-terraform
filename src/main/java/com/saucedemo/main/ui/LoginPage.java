package com.saucedemo.main.ui;

import net.serenitybdd.screenplay.targets.Target;

public class LoginPage {
    public static final Target USERNAME_INPUT = Target.the("entrada de texto usuario").locatedBy("#user-name");
    public static final Target PASSWORD_INPUT = Target.the("entrada de texto contrasenia").locatedBy("#password");
    public static final Target LOGIN_BUTTON = Target.the("boton iniciar sesion").locatedBy("#login-button");
    public static final Target INVENTORY_LIST = Target.the("inventario de articulos").locatedBy("#inventory_container");


}
